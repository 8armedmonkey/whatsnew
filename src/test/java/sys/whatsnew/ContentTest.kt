package sys.whatsnew

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import sys.whatsnew.FakeActivationCheckingService.setUpIsFulfilled
import sys.whatsnew.FakeActivationCheckingService.setUpIsUnfulfilled
import java.util.*

class ContentTest {

    @Test
    fun whenAllActivationConditionsAreFulfilledThenItShouldBeActive() {
        val activationCondition1 = mock<ActivationCondition>()
        val activationCondition2 = mock<ActivationCondition>()
        val activationCheckingService = mock<ActivationCheckingService>()
        val content = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = false,
                activationConditions = listOf(activationCondition1, activationCondition2),
                onSeenCallback = mock()
        )

        setUpIsFulfilled(activationCheckingService, activationCondition1)
        setUpIsFulfilled(activationCheckingService, activationCondition2)

        assertTrue(content.isActive(activationCheckingService))
    }

    @Test
    fun whenNotAllActivationConditionsAreFulfilledThenItShouldBeInactive() {
        val activationCondition1 = mock<ActivationCondition>()
        val activationCondition2 = mock<ActivationCondition>()
        val content = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = false,
                activationConditions = listOf(activationCondition1, activationCondition2),
                onSeenCallback = mock()
        )
        val activationCheckingService = mock<ActivationCheckingService>()

        setUpIsUnfulfilled(activationCheckingService, activationCondition1)
        setUpIsFulfilled(activationCheckingService, activationCondition2)

        assertFalse(content.isActive(activationCheckingService))
    }

    @Test
    fun whenActivationConditionsAreEmptyThenItShouldBeInactive() {
        val activationCheckingService = mock<ActivationCheckingService>()
        val content = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = false,
                activationConditions = listOf(),
                onSeenCallback = mock()
        )

        assertFalse(content.isActive(activationCheckingService))
    }

    @Test
    fun whenMarkAsSeenThenItShouldMarkItselfAsSeen() {
        val activationCondition = mock<ActivationCondition>()
        val content = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = false,
                activationConditions = listOf(activationCondition),
                onSeenCallback = mock()
        )

        content.markAsSeen()

        assertTrue(content.hasBeenSeen())
    }

    @Test
    fun whenMarkAsSeenThenItShouldTriggerOnSeenCallback() {
        val activationCondition = mock<ActivationCondition>()
        val onSeenCallback = mock<() -> Unit>()
        val content = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = false,
                activationConditions = listOf(activationCondition),
                onSeenCallback = onSeenCallback
        )

        content.markAsSeen()

        verify(onSeenCallback).invoke()
    }

}