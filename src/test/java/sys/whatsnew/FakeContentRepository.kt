package sys.whatsnew

import com.nhaarman.mockitokotlin2.whenever

object FakeContentRepository {

    fun setUpRetrieveUnseen(
            contentRepository: ContentRepository,
            unseenContents: List<Content>
    ) {
        whenever(contentRepository.retrieveUnseen()).thenReturn(unseenContents)
    }

}