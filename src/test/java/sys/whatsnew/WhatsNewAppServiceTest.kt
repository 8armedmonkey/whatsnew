package sys.whatsnew

import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import sys.whatsnew.FakeActivationCheckingService.setUpIsFulfilled
import sys.whatsnew.FakeActivationCheckingService.setUpIsUnfulfilled
import java.util.*

class WhatsNewAppServiceTest {

    private lateinit var contentRepository: ContentRepository
    private lateinit var whatsNewAppService: WhatsNewAppService
    private lateinit var activationCheckingService: ActivationCheckingService

    @Before
    fun setUp() {
        contentRepository = mock()
        activationCheckingService = mock()
        whatsNewAppService = WhatsNewAppService(
                contentRepository,
                activationCheckingService
        )
    }

    @Test
    fun whenCheckingActiveContentsExistThenItShouldCorrectlyIndicateStatus() {
        val activationCondition1 = mock<ActivationCondition>()
        val content1 = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = false,
                activationConditions = listOf(activationCondition1),
                onSeenCallback = mock()
        )

        val activationCondition2 = mock<ActivationCondition>()
        val content2 = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = false,
                activationConditions = listOf(activationCondition2),
                onSeenCallback = mock()
        )

        val activationCondition3 = mock<ActivationCondition>()
        val content3 = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = false,
                activationConditions = listOf(activationCondition3),
                onSeenCallback = mock()
        )

        setUpIsFulfilled(activationCheckingService, activationCondition1)
        setUpIsUnfulfilled(activationCheckingService, activationCondition2)
        setUpIsFulfilled(activationCheckingService, activationCondition3)

        FakeContentRepository.setUpRetrieveUnseen(
                contentRepository,
                listOf(content1, content2, content3)
        )

        val hasActiveContents = whatsNewAppService.hasActiveContents()

        assertTrue(hasActiveContents)
    }

    @Test
    fun whenRetrievingActiveContentsThenItShouldNotIncludeInactiveContents() {
        val activationCondition1 = mock<ActivationCondition>()
        val content1 = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = false,
                activationConditions = listOf(activationCondition1),
                onSeenCallback = mock()
        )

        val activationCondition2 = mock<ActivationCondition>()
        val content2 = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = false,
                activationConditions = listOf(activationCondition2),
                onSeenCallback = mock()
        )

        val activationCondition3 = mock<ActivationCondition>()
        val content3 = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = false,
                activationConditions = listOf(activationCondition3),
                onSeenCallback = mock()
        )

        setUpIsFulfilled(activationCheckingService, activationCondition1)
        setUpIsUnfulfilled(activationCheckingService, activationCondition2)
        setUpIsFulfilled(activationCheckingService, activationCondition3)
        FakeContentRepository.setUpRetrieveUnseen(
                contentRepository,
                listOf(content1, content2, content3)
        )

        val contents = whatsNewAppService.retrieveActiveContents()

        assertTrue(contents.contains(content1))
        assertFalse(contents.contains(content2))
        assertTrue(contents.contains(content3))
    }

    @Test
    fun whenRetrievingActiveContentsThenItShouldNotIncludeSeenContents() {
        val activationCondition1 = mock<ActivationCondition>()
        val content1 = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = false,
                activationConditions = listOf(activationCondition1),
                onSeenCallback = mock()
        )

        val activationCondition2 = mock<ActivationCondition>()
        val content2 = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = false,
                activationConditions = listOf(activationCondition2),
                onSeenCallback = mock()
        )

        val activationCondition3 = mock<ActivationCondition>()
        val content3 = FakeContent(
                id = UUID.randomUUID(),
                hasBeenSeen = true,
                activationConditions = listOf(activationCondition3),
                onSeenCallback = mock()
        )

        setUpIsFulfilled(activationCheckingService, activationCondition1)
        setUpIsFulfilled(activationCheckingService, activationCondition2)
        setUpIsFulfilled(activationCheckingService, activationCondition3)
        FakeContentRepository.setUpRetrieveUnseen(
                contentRepository,
                listOf(content1, content2)
        )

        val contents = whatsNewAppService.retrieveActiveContents()

        assertTrue(contents.contains(content1))
        assertTrue(contents.contains(content2))
        assertFalse(contents.contains(content3))
    }

    @Test
    fun whenMarkAsSeenThenItShouldMarkAllSpecifiedContentsAsSeen() {
        val contentId1 = UUID.randomUUID()
        val contentId2 = UUID.randomUUID()
        val contentId3 = UUID.randomUUID()

        whatsNewAppService.markAsSeen(setOf(contentId1, contentId2, contentId3))

        argumentCaptor<UUID>().apply {
            verify(contentRepository, times(3)).markAsSeen(capture())

            assertTrue(allValues.contains(contentId1))
            assertTrue(allValues.contains(contentId2))
            assertTrue(allValues.contains(contentId3))
        }
    }

}