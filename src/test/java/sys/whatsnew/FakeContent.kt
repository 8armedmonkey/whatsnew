package sys.whatsnew

import java.util.*

class FakeContent(
        id: UUID,
        hasBeenSeen: Boolean,
        activationConditions: List<ActivationCondition>,
        onSeenCallback: () -> Unit
) : Content(id, hasBeenSeen, activationConditions, onSeenCallback)