package sys.whatsnew

import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.whenever

object FakeActivationCheckingService {

    fun setUpIsFulfilled(
            activationCheckingService: ActivationCheckingService,
            activationCondition: ActivationCondition
    ) {
        whenever(activationCheckingService.isFulfilled(eq(activationCondition))).thenReturn(true)
    }

    fun setUpIsUnfulfilled(
            activationCheckingService: ActivationCheckingService,
            activationCondition: ActivationCondition
    ) {
        whenever(activationCheckingService.isFulfilled(eq(activationCondition))).thenReturn(false)
    }

}