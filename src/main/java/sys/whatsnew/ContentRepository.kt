package sys.whatsnew

import java.util.*

interface ContentRepository {

    fun retrieveUnseen(): List<Content>

    fun markAsSeen(id: UUID)

}