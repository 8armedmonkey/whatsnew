package sys.whatsnew

interface ActivationCheckingService {

    fun isFulfilled(activationCondition: ActivationCondition): Boolean

}