package sys.whatsnew

data class ToggleEnabledCondition(
        val toggleId: String
) : ActivationCondition