package sys.whatsnew

import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

abstract class Content(
        val id: UUID,
        hasBeenSeen: Boolean,
        private val activationConditions: List<ActivationCondition>,
        private val onSeenCallback: () -> Unit
) {

    private val hasBeenSeen: AtomicBoolean = AtomicBoolean(hasBeenSeen)

    internal fun isActive(activationCheckingService: ActivationCheckingService): Boolean =
            activationConditions.isNotEmpty()
                    && activationConditions.all { activationCheckingService.isFulfilled(it) }

    internal fun hasBeenSeen(): Boolean = hasBeenSeen.get()

    internal fun markAsSeen() {
        if (!hasBeenSeen.getAndSet(true)) {
            onSeenCallback.invoke()
        }
    }

}