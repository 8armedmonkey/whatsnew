package sys.whatsnew

import java.util.*

class WhatsNewAppService(
        private val contentRepository: ContentRepository,
        private val activationCheckingService: ActivationCheckingService
) {

    fun hasActiveContents(): Boolean =
            retrieveActiveContents().isNotEmpty()

    fun retrieveActiveContents(): List<Content> =
            contentRepository.retrieveUnseen().filter {
                it.isActive(activationCheckingService)
            }

    fun markAsSeen(contentIds: Set<UUID>) {
        contentIds.forEach { contentRepository.markAsSeen(it) }
    }

}