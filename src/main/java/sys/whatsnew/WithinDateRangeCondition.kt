package sys.whatsnew

import java.util.*

data class WithinDateRangeCondition(
        val startDate: Date?,
        val endDate: Date?
) : ActivationCondition